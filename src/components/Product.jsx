import { useEffect, useState } from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import Logo from '../assets/logo-login.png'


const ProductPage = ({ token, onLogout}) => {
  const [products, setProducts] = useState([]);
 

  const handleLogout = () => {
 
    onLogout();
  };
  useEffect(() => {
    const fetchProducts = async () => {
      try {
        const response = await axios.get('https://dummyjson.com/products', {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        setProducts(response.data.products);
        
      } catch (error) {
        console.error('Error fetching products:', error);
      }
    };

    fetchProducts();
  }, [token]);


  

  return (
    <>
     
      <header className="text-gray-600 body-font">
  <div className="container mx-auto flex justify-between p-5 flex-col md:flex-row items-center">
   
      <img src={Logo} alt="" className='h-14 w-18'/>
     
    
   
    <button className="inline-flex items-center bg-gray-100 border-0 py-1 px-3 focus:outline-none hover:bg-gray-200 rounded text-base mt-4 md:mt-0" onClick={handleLogout}>Logout
      <svg fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" className="w-4 h-4 ml-1" viewBox="0 0 24 24">
        <path d="M5 12h14M12 5l7 7-7 7"></path>
      </svg>
    </button>
  </div>
</header>

      <section className="text-gray-600 body-font">
        
  <div className="container px-5 py-10 mx-auto">
  <h1 className="title-font sm:text-4xl text-3xl mb-4 font-medium text-gray-900">Product list</h1>
  
    <div className="flex flex-wrap -m-4">
    {products?.map((product) => (
           <div key={product.id}className="lg:w-1/4 md:w-1/2 p-4 w-full">
           <a className="block relative h-48 rounded overflow-hidden">
             <img alt="ecommerce" className="object-cover object-center w-full h-full block" src={product.thumbnail} />
           </a>
           <div className="mt-4">
             <h3 className="text-gray-500 text-xs tracking-widest title-font mb-1">{product.title}</h3>
             <h2 className="text-gray-900 title-font text-lg font-medium ">{product.description}</h2>
             <p className="mt-1">$ {product.price}</p>
            
            
           </div>
         </div>
        ))}
      
    </div>
  </div>
</section>
    </>
  );
};

ProductPage.propTypes = {
  token: PropTypes.string.isRequired,
  onLogout: PropTypes.func.isRequired,
};

export default ProductPage;
