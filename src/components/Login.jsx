import { useState } from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import HeaderLogo from '../assets/header-login.png'
import Logo from '../assets/logo-login.png'
import Swal from 'sweetalert2';

const Login = ({ onLogin }) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleLogin = async (e) => {
    e.preventDefault();
    if (username.trim() === '' || password.trim() === '') {
      Swal.fire('Error', 'User ID dan atau Password anda belum diisi.', 'error');
      return;
    }
    try {
      const response = await axios.post('https://dummyjson.com/auth/login', { username, password });
      const { token } = response.data;
      onLogin(token);
    } catch (error) {
      Swal.fire('Error', 'Incorrect username or password', 'error');
    }
  };

  return (
    <>
   <div className="flex flex-col md:flex-row  md:hidden">
  {/* <img
    alt=""
    src={HeaderLogo}
    className="w-32 h-32  object-center md:object-none md:ml-auto md:mt-0 md:mr-8"
  />

  <div className="flex items-center justify-center md:justify-start">
    <img
      alt=""
      src={Logo}
      className="object-center w-28 h-28"
    />
  </div> */}
  <div className="flex flex-row items-center md:hidden">
  <img
    alt=""
    src={HeaderLogo}
    className="w-32 h-32 object-center"
  />

  <img
    alt=""
    src={Logo}
    className="object-center w-20 h-20 ml-4"
  />
</div>

  <div className="md:w-1/3 max-w-sm  mt-10  p-8">
      
  <p className="my-2 text-left font-bold text-xl">Sign In</p>
      <p className="mb-4 text-left text-xs ">Please Sign In To Continue</p>
      <form onSubmit={handleLogin}>
      <label className="block text-gray-700 text-sm font-bold mb-2" >
       User ID
      </label>
      <input className="text-sm w-full py-1 appearance-none bg-transparent border-b"  type="text"
      placeholder="User ID"
      value={username}
      onChange={(e) => setUsername(e.target.value)} />
       <label className="block text-gray-700 text-sm font-bold my-2" >
       Password
      </label>
       <input className="text-sm w-full py-1 appearance-none bg-transparent border-b" type="password"
      placeholder="Password"
      value={password}
      onChange={(e) => setPassword(e.target.value)}/>
      
      <div className="text-right md:text-left">
        <button className="mt-4 bg-violet-900 hover:bg-blue-700 px-4 py-2 text-white uppercase rounded-full text-xs tracking-wider" type="submit">Login</button>
      </div>
      <div className="mt-4 font-semibold text-sm text-slate-500 text-center md:text-left">
        Dont have an account? <a className="text-red-600 hover:underline hover:underline-offset-4" href="#">Sign Up</a>
      </div>
      </form>
    </div>
</div>

  <section className='h-screen flex flex-col md:flex-row justify-center space-y-10 md:space-y-0 md:space-x-16 items-center my-2 mx-5 md:mx-0 md:my-0 hidden md:flex'> 
    
    <div className="md:w-1/3 max-w-sm hidden md:block " >
        <img
          src={Logo}
          alt="" />
      </div>
   
      <div className="md:w-1/3 max-w-sm bg-gray-100 rounded-lg p-8 hidden md:block ">
      
        <div className="my-5 flex items-center before:mt-0.5 before:flex-1 before:border-t before:border-neutral-300 after:mt-0.5 after:flex-1 after:border-t after:border-neutral-300">
          <p className="mx-4 mb-0 text-center font-semibold text-slate-500">Please Sign In To Continue</p>
        </div>
        <form onSubmit={handleLogin}>
        <input className="text-sm w-full px-4 py-2 border border-solid border-gray-300 rounded"  type="text"
        placeholder="Username"
        value={username}
        onChange={(e) => setUsername(e.target.value)} />
         <input className="text-sm w-full px-4 py-2 border border-solid border-gray-300 rounded mt-4" type="password"
        placeholder="Password"
        value={password}
        onChange={(e) => setPassword(e.target.value)}/>
        
        <div className="text-right md:text-left">
          <button className="mt-4 bg-violet-900 hover:bg-blue-700 px-4 py-2 text-white uppercase rounded text-xs tracking-wider" type="submit">Login</button>
        </div>
        <div className="mt-4 font-semibold text-sm text-slate-500 text-center md:text-left">
          Dont have an account? <a className="text-red-600 hover:underline hover:underline-offset-4" href="#">Register</a>
        </div>
        </form>
       
       
      
      </div>
    
    </section>
  </>
    
  
  );
};

Login.propTypes = {
  onLogin: PropTypes.func.isRequired,
};

export default Login;
