import { useState } from 'react'
import { Routes, Route } from "react-router-dom"
import Login from './components/Login'

import ProductPage from './components/Product'


function App() {
  const [token, setToken] = useState('');

  const handleLogin = (token) => {
    setToken(token);
  };

  const handleLogout = () => {
    setToken('');
  };

  return (
    <>
    
      <div>
        {token ? (
          <>
          
            <Routes>
              <Route exact path="/"  element={ <ProductPage token={token} onLogout={handleLogout}/>} />
             
            </Routes>
          </>
        ) : (
          <Login onLogin={handleLogin} />
        )}
      </div>
    
    </>
  )
}

export default App
